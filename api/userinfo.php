<?php
 include "../index.php";

 function getcode()
 {
 	$code=$_GET['code'];
	$url="https://api.weixin.qq.com/sns/jscode2session?appid={$GLOBALS['appid']}&secret={$GLOBALS['appsecret']}&js_code={$code}&grant_type=authorization_code";
	$result=get_json($url);
	echo $result;
 }

 function get_userinfo(){
 	$userinfo=json_decode($_GET['userInfo'],true);
 	$userinfo['openid']=$_GET['openid'];
 	$openid=$userinfo['openid'];
 	$sql="select count(*) as count from stu_user where openid='{$openid}'";
 	$data=fetchOne($sql);
 	if($data['count']==0){
 		$result=[
 			'openid'=>$userinfo['openid'],
 			'nickname'=>$userinfo['nickName'],
 			'sex'=>$userinfo['gender'],
 			'headimgurl'=>$userinfo['avatarUrl']
 		];
 		insert($result,'stu_user');
 	}
 }

 function test()
 {
 	$sql="select * from stu_user";
 	$data=fetchOne($sql);
 	p($data);
 }

 function getUser()
 {
 	$openid=$_GET['openid'];
 	$sql="SELECT * FROM stu_user WHERE openid='{$openid}'";
 	$data=fetchOne($sql);
 	echo json($data);
 }

 function updateUser()
{
	$openid=$_POST['openid'];
	$column=$_POST['column'];
	$value=$_POST['value'];
	$data[$column]=$value;
	if(update($data,'stu_user',"openid='{$openid}'","{$column}={$value}"))
		echo json(['code'=>1,'message'=>'操作成功']);
	else
		echo json(['code'=>1,'message'=>'操作失败']);
}


function getfoodclass()
{
	$restaurant_id=$_GET['restaurant_id'];
	$sql="select * from food_class where restaurant_id={$restaurant_id}";
	$data=fetchAll($sql);
	echo json(['code'=>1,'message'=>'获取成功','data'=>$data]);
}

function getfood()
{
	$food_class_id=$_GET['food_class_id'];
	$sql="SELECT * FROM food WHERE food_class_id={$food_class_id}";
	$data=fetchAll($sql);
	for($i=0;$i<count($data);$i++)
		$data[$i]['food_img']= str_replace('\\','/',$data[$i]['food_img']);
	echo json(['code'=>1,'message'=>'获取成功','data'=>$data]);
}



function ordersearch()
{
	$openid=$_GET['openid'];
	$sql="SELECT * FROM `order` join order_food on order.order_id=order_food.order_id join restaurant on order.restaurant_id=restaurant.restaurant_id join  food on order_food.food_id=food.food_id where order.openid={$openid}";
	$data=fetchAll($sql);
	echo json(['code'=>1,'message'=>'获取成功','data'=>$data]);
}

function getrestaurant()
{
	header('Content-Type: application/json');
	$sql="SELECT * FROM restaurant";
	$data=fetchAll($sql);
	for($i=0;$i<count($data);$i++)
		$data[$i]['restaurant_img']= str_replace('\\','/',$data[$i]['restaurant_img']);
	echo json(['code'=>1,'message'=>'获取成功','data'=>$data]);
}

function orderfood()
{
	$data=$_POST;
	p($data);
}