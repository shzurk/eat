<?php

function p($arr)
{
	if(is_bool($arr))
	{
		var_dump($arr);
	}
	else if(is_null($arr))
	{
		var_dump(NULL);
	}
	else
	{
		echo '<pre style="border: 1px solid rgb(204, 204, 204); padding: 10px; margin-top: 5px; margin-bottom: 5px; line-height: 1.4; font-size: 0.8em; font-family: Menlo, Monaco, Consolas;  background-color: rgb(245, 245, 245);">' . print_r($arr,true) . '</pre>';
	}
}

/**
     * 格式化输出json
     * @param string $json The JSON to make pretty 
     * @param bool $html Insert nonbreaking spaces and <br />s for tabs and linebreaks 
     * @return string The prettified output
*/ 
function p_json($json, $html = false) {
        $tabcount = 0;  
        $result = '';  
        $inquote = false;  
        $ignorenext = false;  
 
        if ($html) {  
            $tab = "&nbsp;&nbsp;&nbsp;";  
            $newline = "<br/>";  
        } else {  
            $tab = "\t";  
            $newline = "\n";  
        }  
 
        for($i = 0; $i < strlen($json); $i++) {  
            $char = $json[$i];  
 
            if ($ignorenext) {  
                $result .= $char;  
                $ignorenext = false;  
            } else {  
                switch($char) {  
                    case '{':  
                        $tabcount++;  
                        $result .= $char . $newline . str_repeat($tab, $tabcount);  
                        break;  
                    case '}':  
                        $tabcount--;  
                        $result = trim($result) . $newline . str_repeat($tab, $tabcount) . $char;  
                        break;  
                    case ',':  
                        $result .= $char . $newline . str_repeat($tab, $tabcount);  
                        break;  
                    case '"':  
                        $inquote = !$inquote;  
                        $result .= $char;  
                        break;  
                    case '\\':  
                        if ($inquote) $ignorenext = true;  
                        $result .= $char;  
                        break;  
                    default:  
                        $result .= $char;  
                }  
            }  
        }  
 
        return $result;  
} 


/**
 *
 * 使用特定function对数组中所有元素做处理
 * @param string &$array  要处理的字符串
 * @param string $function 要执行的函数
 * @return boolean $apply_to_keys_also  是否也应用到key上
 * @access public
 *
 */
function arrayRecursive(&$array, $function, $apply_to_keys_also = false)
{
    static $recursive_counter = 0;
    if (++$recursive_counter > 1000) {
        die('possible deep recursion attack');
    }
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            arrayRecursive($array[$key], $function, $apply_to_keys_also);
        } else {
            $array[$key] = $function($value);
        }

        if ($apply_to_keys_also && is_string($key)) {
            $new_key = $function($key);
            if ($new_key != $key) {
                $array[$new_key] = $array[$key];
                unset($array[$key]);
            }
        }
    }
    $recursive_counter--;
}

/**
 *
 * 将数组转换为JSON字符串（兼容中文）
 * @param array $array  要转换的数组
 * @return string  转换得到的json字符串
 * @access public
 *
 */

function json($array) 
{
 arrayRecursive($array, 'urlencode', true);
 $json = json_encode($array);
 return urldecode($json);
}


function halt($error){
	p($error);
	die;
}

// 说明：获取完整URL
 
function curPageURL() 
{
  $pageURL = 'http';
 
  if ($_SERVER["HTTPS"] == "on") 
  {
    $pageURL .= "s";
  }
  $pageURL .= "://";
 
  if ($_SERVER["SERVER_PORT"] != "80") 
  {
    $pageURL .= $_SERVER["SERVER_NAME"] .  $_SERVER["REQUEST_URI"];
  } 
  else
  {
    $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
  }
  return $pageURL;
}


function get_json($url)
{
    $curlobj=curl_init();
    curl_setopt($curlobj, CURLOPT_URL, $url);
    curl_setopt($curlobj, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curlobj, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlobj, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlobj, CURLOPT_HEADER, 0);
    $result=curl_exec($curlobj);
    curl_close($curlobj);
    return $result;
}

/**
 * 插入记录的操作
 * @param array $array
 * @param string $table
 * @return boolean
 */
function insert($array,$table){
    $keys=join(',',array_keys($array));
    $values="'".join("','", array_values($array))."'";
    $sql="insert into {$table}({$keys}) VALUES ({$values})";
    $res=mysql_query($sql);
    if($res){
        return mysql_insert_id();
    }else{
        return false;
    }
}


/**
 * MYSQL更新操作
 * @param array $array
 * @param string $table
 * @param string $where
 * @return number|boolean
 */
function update($array,$table,$where=null){
    $sets="";
    foreach ($array as $key=>$val){
        $sets.=$key."='".$val."',";
    }
    $sets=rtrim($sets,','); //去掉SQL里的最后一个逗号
    $where=$where==null?'':' WHERE '.$where;
    $sql="UPDATE {$table} SET {$sets} {$where}";
    $res=mysql_query($sql);
    if ($res){
        return mysql_affected_rows();
    }else {
        return false;
    }
}


/**
 * 删除记录的操作
 * @param string $table
 * @param string $where
 * @return number|boolean
 */
function delete($table,$where=null){
    $where=$where==null?'':' WHERE '.$where;
    $sql="DELETE FROM {$table}{$where}";
    $res=mysql_query($sql);
    if ($res){
        return mysql_affected_rows();
    }else {
        return false;
    }
}



/**
 * 查询一条记录
 * @param string $sql
 * @param string $result_type
 * @return boolean
 */
function fetchOne($sql,$result_type=MYSQL_ASSOC){
    $result=mysql_query($sql);
    if ($result && mysql_num_rows($result)>0){
        return mysql_fetch_array($result,$result_type);
    }else {
        return false;
    }
}





/**
 * 得到表中的所有记录
 * @param string $sql
 * @param string $result_type
 * @return boolean
 */
function fetchAll($sql,$result_type=MYSQL_ASSOC){
    $result=mysql_query($sql);
    if ($result && mysql_num_rows($result)>0){
        while ($row=mysql_fetch_array($result,$result_type)){
            $rows[]=$row;
        }
        return $rows;
    }else {
        return false;
    }
}


/**取得结果集中的记录的条数
 * @param string $sql
 * @return number|boolean
 */
function getTotalRows($sql){
    $result=mysql_query($sql);
    if($result){
        return mysql_num_rows($result);
    }else {
        return false;
    }
    
}

/**释放结果集
 * @param resource $result
 * @return boolean
 */
function  freeResult($result){
    return  mysql_free_result($result);
}
