const app = getApp();
Page({
  data:{
    userInfo: {},
    hasUserInfo: false,
    relInfo:null
  },
  onLoad: function () {
    this.setData({
      userInfo: app.userData.userInfo,
      hasUserInfo: true
    });
    var parameter = {
      openid: getApp().userData.openid
    }
    this.ajax('/api/userinfo.php?a=getUser', parameter, 'GET');
  },
  ajax: function (url, parameter, method){
    var thispage = this;
    wx.request({
      //url: getApp().data.app_servsers + '/api/userinfo.php?a=getUser',
      url: getApp().data.app_servsers + url,
      method: method,
      data: parameter,
      success: function (res) {
        thispage.setData({
          relInfo: res.data
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '网络错误',
          mask: true
        })
      }
    })
  },
  ajaxRefresh: function (url, parameter, method) {
    var thispage = this;
    wx.request({
      //url: getApp().data.app_servsers + '/api/userinfo.php?a=getUser',
      url: getApp().data.app_servsers + url,
      method: method,
      data: parameter,
      success: function (res) {
        thispage.setData({
          relInfo: res.data
        })
        wx.stopPullDownRefresh();
      },
      fail: function (res) {
        wx.showToast({
          title: '网络错误',
          mask: true
        })
      }
    })
  },
  jump_url: function (event) {
    wx.navigateTo({
      url: event.currentTarget.id
    })
  },
  onPullDownRefresh: function () {
    var parameter = {
      openid: getApp().userData.openid
    }
    this.ajaxRefresh('/api/userinfo.php?a=getUser', parameter, 'GET');
  }
});