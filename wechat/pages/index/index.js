
var file_servsers = getApp().data.file_servsers;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    img: [
      file_servsers + 'stbg.jpg',
      file_servsers + 'stbg2.jpg',
      file_servsers + 'stbg3.jpg',
      file_servsers + 'stbg4.jpg',
      file_servsers + 'stbg5.jpg',
      file_servsers + 'stbg6.jpg',
    ],
    file_servsers: getApp().data.upload_servsers,
    restaurant:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thispage = this;
    wx.request({
      url: getApp().data.app_servsers + '/api/userinfo.php?a=getrestaurant',
      data: {},
      header: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        console.log(res.data[0]);
        if(res.data.code == 1){
          thispage.setData({
            restaurant: res.data.data
          })
        }else{
          wx.showToast({
            title: '网络错误',
            mask: true
          })
        }
        
      },
      fail: function (res) {
        wx.showToast({
          title: '网络错误',
          mask: true
        })
      }
    })
    wx.removeStorage({
      key: 'order',
      success: function (res) {}
    })

    wx.removeStorage({
      key: 'order_mess',
      success: function (res) { }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  jump_url: function (event) {
    wx.navigateTo({
      url: event.currentTarget.id
    })
  }
})