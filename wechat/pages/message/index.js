// pages/message/index.js

var file_servsers = getApp().data.file_servsers;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowWidth: wx.getSystemInfoSync().windowWidth,//图片宽度
    windowHeight: wx.getSystemInfoSync().windowHeight,//图片宽度
    file_servsers: getApp().data.upload_food_img,
    foods: [
      file_servsers + 'food.jpg',
      file_servsers + 'food.jpg',
      file_servsers + 'food.jpg',
      file_servsers + 'food.jpg',
      file_servsers + '-.png',
      file_servsers + '+.png'
    ],
    bottom_img:[
      file_servsers + 'back.png',
      file_servsers + 'js.png',
      file_servsers + 'gwc.png'
    ],
    id:'',
    food_class:'',
    food:'',
    menuclass:-1,
    totle:0,
    totle_price:0,
    order:[],
    restaurant_id:-1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thispage = this;

    thispage.setData({
      restaurant_id: options.id
    })

    wx.request({
      url: getApp().data.app_servsers + '/api/userinfo.php?a=getfoodclass',
      data: {
        restaurant_id:options.id
      },
      header: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        console.log(res.data[0]);
        if (res.data.code == 1) {
          thispage.setData({
            food_class: res.data.data
          })
        } else {
          wx.showToast({
            title: '网络错误',
            mask: true
          })
        }
        this.one_first(this.data.food_class[0])
      },
      fail: function (res) {
        wx.showToast({
          title: '网络错误',
          mask: true
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  jump_url: function (event) {
    if (this.data.totle_price!=0){
      wx.navigateTo({
        url: event.currentTarget.id
      })
    }else{
      wx.showToast({
        title: '还未选择',
        mask: true
      })
    }
  },
  one_first:function(id){
    var id = id
    var thispage = this;
    wx.request({
      url: getApp().data.app_servsers + '/api/userinfo.php?a=getfood',
      data: {
        food_class_id: id
      },
      header: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 1) {
          thispage.setData({
            food: res.data.data,
            'totle': 0,
            'totle_price': 0
          })
          thispage.setData({
            menuclass: id
          })

        } else {
          wx.showToast({
            title: '网络错误',
            mask: true
          })
        }
      },
      fail: function (res) {
        wx.showToast({
          title: '网络错误',
          mask: true
        })
      }
    })
  },
  food: function (event){
    var id = event.currentTarget.id
    var thispage = this;
    wx.request({
      url: getApp().data.app_servsers + '/api/userinfo.php?a=getfood',
      data: {
        food_class_id: id
      },
      header: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 1) {
          thispage.setData({
            food: res.data.data,
            'totle': 0,
            'totle_price': 0
          })
          thispage.setData({
            menuclass: id
          })
          
        } else {
          wx.showToast({
            title: '网络错误',
            mask: true
          })
        }
      },
      fail: function (res) {
        wx.showToast({
          title: '网络错误',
          mask: true
        })
      }
    })
  },
  add_number: function (event){
    var id = event.currentTarget.id

    var foods = 'food[' + id + '].number'
    this.setData({
      [foods]: parseInt(this.data.food[id].number)+1,
      'totle': this.data.totle +1,
      'totle_price': this.data.totle_price + parseInt(this.data.food[0].food_price)
    })
    
    this.data.order.push({
      food_name: this.data.food[id].food_name,
      food_id: this.data.food[id].food_id,
      food_price: this.data.food[id].food_price
    })

    this.setData({
      order: this.data.order
    })

    wx.setStorage({
      key: "order",
      data: this.data.order
    })

    wx.setStorage({
      key: "order_mess",
      data: {
        totle_price: this.data.totle_price,
        restaurant_id: this.data.restaurant_id
      }
      
    })

  },
  del_number: function (event) {
    var id = event.currentTarget.id
    var foods = 'food[' + id + '].number'
    this.setData({
      [foods] : parseInt(this.data.food[id].number) - 1,
      'totle': this.data.totle - 1,
      'totle_price': this.data.totle_price - parseInt(this.data.food[0].food_price)
    })

    this.data.order.pop({
      food_name: this.data.food[id].food_name,
      food_id: this.data.food[id].food_id,
      food_price: this.data.food[id].food_price
    })

    this.setData({
      order: this.data.order
    })

    wx.setStorage({
      key: "order",
      data: this.data.order
    })

    wx.setStorage({
      key: "order_mess",
      data: {
        totle_price: this.data.totle_price,
        restaurant_id: this.data.restaurant_id
      }
    })
  }
})