// pages/submit/index.js
var file_servsers = getApp().data.file_servsers;
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    bottom_img: [
      file_servsers + 'back.png',
      file_servsers + 'js.png',
      file_servsers + 'gwc.png',
      file_servsers + 't1.png',
      file_servsers + 'order.png'
    ],
    order_list:[
      {

      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var thispage = this;
    wx.request({
      url: getApp().data.app_servsers + '/api/userinfo.php?a=get_order',
      data: {
        openid: app.userData.openid
      },
      header: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        console.log(res.data[0]);
        if (res.data.code == 1) {
          // thispage.setData({
          //   //food_class: res.data.data
          // })
        } else {
          wx.showToast({
            title: '网络错误',
            mask: true
          })
        }

      },
      fail: function (res) {
        wx.showToast({
          title: '网络错误',
          mask: true
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  jump_url: function (event) {
    wx.navigateTo({
      url: event.currentTarget.id
    })
  },
  confirm:function(){
    wx.showToast({
      title: '确认成功',
      mask: true
    })
  },
  cancel: function () {
    wx.showToast({
      title: '取消成功',
      mask: true
    })
  }
})