// pages/submit/index.js
var file_servsers = getApp().data.file_servsers;
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    bottom_img: [
      file_servsers + 'back.png',
      file_servsers + 'js.png',
      file_servsers + 'gwc.png',
      file_servsers + 't1.png'
    ],
    food_list:{},
    order_mess: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo: app.userData.userInfo,
    })
    var thisdata = this;
    wx.getStorage({
      key: 'order',
      success: function (res) {
        thisdata.setData({
          food_list: res.data
        })
      }
    })

    wx.getStorage({
      key: 'order_mess',
      success: function (res) {
        thisdata.setData({
          order_mess: res.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  jump_url: function (event) {
    wx.navigateTo({
      url: event.currentTarget.id
    })
  },
  jump_url_tab: function (event) {


    // wx.request({
    //   url: getApp().data.app_servsers + '/api/userinfo.php?a=save_order',
    //   method:"POST",
    //   data: JSON.stringify(this.data.food_list),
    //   header: {
    //     'content-type': 'application/json' // 默认值
    //   },
    //   success: function (res) {
    //     console.log(res.data)
    //   }
    // })

    //console.log(JSON.stringify(this.data.food_list));

    wx.switchTab({
      url: event.currentTarget.id
    })
  }
})