App({
  data: {
    app_servsers: "https://kuaichi.shzurk.com",
    file_servsers: "https://kuaichi.shzurk.com/Public/Images/",//图片数据
    upload_servsers: "https://kuaichi.shzurk.com/easyeat/public/uploads/canteen/",//图片数据
    upload_food_img: "https://kuaichi.shzurk.com/easyeat/public/uploads/foodimg/",//图片数据
  },
  userData: {
    userInfo: null,
    openid: null
  },
  onLaunch: function () {
    // 登录
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.userData.userInfo = res.userInfo;

              wx.login({
                success: res => {
                  // 发送 res.code 到后台换取 openId, sessionKey, unionId
                  if (res.code) {
                    //发起网络请求
                    wx.request({
                      url: this.data.app_servsers + '/api/userinfo.php?a=getcode',
                      data: {
                        code: res.code
                      },
                      success: function (res) {
                        console.log(res.data.openid);
                        getApp().userData.openid = res.data.openid;
                        /*发送用户信息*/
                        wx.request({
                          url: getApp().data.app_servsers + '/api/userinfo.php?a=get_userinfo',
                          data: getApp().userData
                        })

                      },
                      fail: function (res) {
                        wx.showToast({
                          title: '网络错误',
                          mask: true
                        })
                      },
                    })
                  } else {
                    console.log('获取用户登录态失败！' + res.errMsg)
                  }
                }
              })
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        } else {
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.userData.userInfo = res.userInfo;
              wx.login({
                success: res => {
                  // 发送 res.code 到后台换取 openId, sessionKey, unionId
                  if (res.code) {
                    //发起网络请求
                    wx.request({
                      url: this.data.app_servsers + '/api/userinfo.php?a=getcode',
                      data: {
                        code: res.code
                      },
                      success: function (res) {
                        console.log(res.data.openid);
                        getApp().userData.openid = res.data.openid;

                        wx.request({
                          url: getApp().data.app_servsers + '/api/userinfo.php?a=get_userinfo',
                          data: getApp().userData
                        })
                      },
                      fail: function (res) {
                        wx.showToast({
                          title: '网络错误',
                          mask: true
                        })
                      },
                    })
                  } else {
                    console.log('获取用户登录态失败！' + res.errMsg)
                  }
                }
              })
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  }
})