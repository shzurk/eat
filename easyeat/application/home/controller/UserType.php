<?php
namespace app\home\controller;
use think\Controller;
use think\Db;

class UserType extends Controller
{

	public function index()
	{
		$data=Db::table('admin_user')->join('restaurant','admin_user.restaurant_id=restaurant.restaurant_id')->select();
		$resdata=Db::table('restaurant')->select();
		$this->assign('resdata',$resdata);
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function addfun()
	{
		$data=input();
		if($data['username']=='root')
			return ['code'=>0,'message'=>'不能使用系统用户名'];
		if(Db::table('admin_user')->where('username',$data['username'])->count())
			return ['code'=>0,'message'=>'用户名已存在'];
		if(Db::table('admin_user')->insert($data)){
			return ['code'=>1,'message'=>'添加成功'];
		}
		else
			return ['code'=>0,'message'=>'添加失败'];
	}

	public function deletefun()
	{
		$user_id=input('user_id');
		if(Db::table('admin_user')->where('user_id',$user_id)->delete())
			return ['code'=>1,'message'=>'删除成功'];
		else
			return ['code'=>0,'message'=>'删除失败'];
	}

	public function update()
	{
		$user_id=input('user_id');
		$data=Db::table('admin_user')->join('restaurant','admin_user.restaurant_id=restaurant.restaurant_id')->where('user_id',$user_id)->select();
		$resdata=Db::table('restaurant')->select();
		$this->assign('resdata',$resdata);
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function updatefun()
	{
		$data=input('');
		$user_id=$data['user_id'];
		unset($data['user_id']);
		if(Db::table('admin_user')->where('user_id',$user_id)->update($data))
			return ['code'=>1,'message'=>'修改成功'];
		else
			return ['code'=>0,'message'=>'修改失败'];
	}


}