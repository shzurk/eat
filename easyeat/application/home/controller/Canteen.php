<?php 
namespace app\home\controller;
use think\Controller;
use think\Db;
use think\Session;
class Canteen extends BController
{
	public function index()
	{
		return $this->fetch();
	}
	public function message()
	{
		return $this->fetch();
	}

	public function canteenmessage()
	{
		$data=Db::table('restaurant')->select();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function addfun()
	{
		$data=input();
		$file = request()->file('img');
		if(!isset($file))
			return ['code'=>0,'message'=>'菜品图标不能为空'];
	    $info = $file->validate(['size'=>605678,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads' . DS . 'canteen');
	    if($info){
	    	$data['restaurant_img']=$info->getSaveName();
	    }
	    else
	    	return ['code'=>0,'message'=>$file->getError()];
		if(Db::table('restaurant')->insert($data))
			return ['code'=>1,'message'=>'添加成功','data'=>$data['restaurant_img']];
		else
			return ['code'=>0,'message'=>'添加失败'];
	}

	public function deletefun()
	{
		$restaurant_id=input('restaurant_id');
		if(Db::table('restaurant')->where('restaurant_id',$restaurant_id)->delete())
			return ['code'=>1,'message'=>'删除成功'];
		else
			return ['code'=>0,'message'=>'删除失败'];			
	}

	public function update()
	{
		$restaurant_id=input('restaurant_id');
		$data=Db::table('restaurant')->where('restaurant_id',$restaurant_id)->select();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function updatefun()
	{
		$data=input();
		$restaurant_id=$data['restaurant_id'];
		unset($data['restaurant_id']);
		$restaurant_img=Db::table('restaurant')->where('restaurant_id',$restaurant_id)->value('restaurant_img');
		$file = request()->file('img');
		if(!isset($file))
			$data['restaurant_img']=$restaurant_img;
		else{
		    $info = $file->validate(['size'=>605678,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads' . DS . 'canteen');
		    if($info){
		    	$data['restaurant_img']=$info->getSaveName();
		    }
		    else
		    	return ['code'=>0,'message'=>$file->getError()];
		}
		if(Db::table('restaurant')->where('restaurant_id',$restaurant_id)->update($data))
			return ['code'=>1,'message'=>'修改成功','data'=>$data['restaurant_img']];
		else
			return ['code'=>0,'message'=>'修改失败'];	
	}
}
