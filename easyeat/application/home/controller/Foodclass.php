<?php 
namespace app\home\controller;
use think\Controller;
use think\Db;
use think\Session;
class Foodclass extends BController
{
	public function index()
	{		
		$username=Session::get('username');
		$restaurant_id=Db::table('admin_user')->where('username',$username)->value('restaurant_id');
		$data=Db::table('food_class')->where('restaurant_id',$restaurant_id)->select();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function addfun()
	{
		$data=input();
		$username=Session::get('username');
		$data['restaurant_id']=Db::table('admin_user')->where('username',$username)->value('restaurant_id');
		if(Db::table('food_class')->insert($data)){
			return ['code'=>1,'message'=>'添加成功'];
		}
		else
			return ['code'=>0,'message'=>'添加失败'];
	}

	public function deletefun()
	{
		$food_class_id=input('food_class_id');
		if(Db::table('food_class')->where('food_class_id',$food_class_id)->delete())
			return ['code'=>1,'message'=>'删除成功'];
		else
			return ['code'=>0,'message'=>'删除失败'];			
	}

	public function update()
	{
		$food_class_id=input('food_class_id');
		$restaurant_name=Db::table('food_class')->where('food_class_id',$food_class_id)->value('restaurant_name');
		$this->assign('restaurant_name',$restaurant_name);
		$restaurantdata=Db::table('restaurant')->select();
		$this->assign('restaurantdata',$restaurantdata);
		$data=Db::table('food_class')->where('food_class_id',$food_class_id)->select();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function updatefun()
	{
		$data=input();
		$food_class_id=$data['food_class_id'];
		unset($data['food_class_id']);
		if(Db::table('food_class')->where('food_class_id',$food_class_id)->update($data))
			return ['code'=>1,'message'=>'修改成功'];
		else
			return ['code'=>0,'message'=>'修改失败'];	
	}
}