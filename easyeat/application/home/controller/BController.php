<?php
namespace app\home\controller;
use think\Controller;
use think\session;
class BController extends Controller
{
    public function _initialize()
    {
        if(empty(session('username')))
            $this->redirect('Login/index');

    }
}