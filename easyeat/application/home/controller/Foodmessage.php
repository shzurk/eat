<?php 
namespace app\home\controller;
use think\Controller;
use think\Db;
use think\Session;
class Foodmessage extends BController
{

public function index()
	{		
		$username=Session::get('username');
		$restaurant_id=Db::table('admin_user')->where('username',$username)->value('restaurant_id');
		$data=Db::table('food')->join('food_class','food.food_class_id=food_class.food_class_id')->where('food.restaurant_id',$restaurant_id)->select();
		$datatype=Db::table('food_class')->where('restaurant_id',$restaurant_id)->select();
		$this->assign('datatype',$datatype);
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function addfun()
	{
		$data=input();
		$file = request()->file('img');
		if(!isset($file))
			return ['code'=>0,'message'=>'菜品图标不能为空'];
	    $info = $file->validate(['size'=>605678,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads' . DS . 'foodimg');
	    if($info){
	    	$data['food_img']=$info->getSaveName();
	    }
	    else
	    	return ['code'=>0,'message'=>$file->getError()];
		$username=Session::get('username');
		$data['restaurant_id']=Db::table('admin_user')->where('username',$username)->value('restaurant_id');
		$data['sell_count']=0;
		if(Db::table('food')->insert($data)){
			return ['code'=>1,'message'=>'添加成功','data'=>$data['food_img']];
		}
		else
			return ['code'=>0,'message'=>'添加失败'];
	}

	public function deletefun()
	{
		$food_id=input('food_id');
		if(Db::table('food')->where('food_id',$food_id)->delete())
			return ['code'=>1,'message'=>'删除成功'];
		else
			return ['code'=>0,'message'=>'删除失败'];			
	}

	public function update()
	{
		$food_id=input('food_id');
		$data=Db::table('food')->where('food_id',$food_id)->select();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function updatefun()
	{
		$data=input();
		$food_id=$data['food_id'];
		unset($data['food_id']);
		$food_img=Db::table('food')->where('food_id',$food_id)->value('food_img');
		$file = request()->file('img');
		if(!isset($file))
			$data['food_img']=$food_img;
	    $info = $file->validate(['size'=>605678,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads' . DS . 'foodimg');
	    if($info){
	    	$data['food_img']=$info->getSaveName();
	    }
		if(Db::table('food')->where('food_id',$food_id)->update($data))
			return ['code'=>1,'message'=>'修改成功','data'=>$data['food_img']];
		else
			return ['code'=>0,'message'=>'修改失败'];	
	}
}