﻿$(function(){
	//文本框失去焦点
	$(".mainForm input").blur(function(){
		$("#mz_Float").css("top","");
	});

//用户栏失去焦点
	$(".user").blur(function(){
		if($(".user").val()==""){
			$(".user").parent().addClass("errorC");
		}
       else
        {
        	$(".user").parent().addClass("checkedN");
        }
	});


	//密码栏失去焦点(mainform1)
	$(".password").blur(function(){
		var Pval = $(".password").val();
		if( Pval =="")
		{
			$(".password").parent().addClass("errorC");
		}
        else
        {
        	$(".password").parent().addClass("checkedN");
        }
	});
	//验证码栏失去焦点
	$(".kapkey").blur(function(){
		reg=/^.*[\u4e00-\u9fa5]+.*$/;
		if( $(".kapkey").val()=="")
		{
			$(".kapkey").parent().addClass("errorC");
			$(".error2").html("请填写验证码");
			$(".error2").css("display","block");
		}
        else if($(".kapkey").val().length<4)
        {   
        	$(".kapkey").parent().addClass("errorC");
            $(".error2").html("验证码长度有误！");
            $(".error2").css("display","block");
        }
        else if(reg.test($(".kapkey").val()))
        {
        	$(".kapkey").parent().addClass("errorC");
            $(".error2").html("验证码里无中文！");
            $(".error2").css("display","block");
        }
        else 
        {
			$(".error2").html("");
            $(".error2").css("display","block");
        	$(".kapkey").parent().addClass("checkedN");
        }
	});
});

