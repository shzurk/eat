/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.19 : Database - canteen
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`canteen` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `canteen`;

/*Table structure for table `admin_user` */

DROP TABLE IF EXISTS `admin_user`;

CREATE TABLE `admin_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '食堂负责人id',
  `openid` varchar(50) NOT NULL COMMENT '微信编号',
  `nickname` varchar(50) NOT NULL COMMENT '昵称',
  `sex` int(1) NOT NULL COMMENT '性别',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `teacher_id` varchar(50) DEFAULT NULL COMMENT '工号',
  `headimgurl` varchar(50) NOT NULL COMMENT '用户头像',
  `restaurant_id` int(11) NOT NULL COMMENT '食堂表',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `admin_user` */

/*Table structure for table `food` */

DROP TABLE IF EXISTS `food`;

CREATE TABLE `food` (
  `food_id` int(11) NOT NULL AUTO_INCREMENT,
  `food_name` varchar(50) NOT NULL COMMENT '菜名',
  `restaurant_id` int(11) NOT NULL COMMENT '食堂id',
  `sell_count` int(11) DEFAULT NULL COMMENT '销售数量',
  `food_img` text COMMENT '菜品图标',
  `food_price` double DEFAULT NULL COMMENT '菜品价格',
  `food_class_id` int(11) DEFAULT NULL COMMENT '分类id',
  PRIMARY KEY (`food_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

/*Data for the table `food` */

/*Table structure for table `food_class` */

DROP TABLE IF EXISTS `food_class`;

CREATE TABLE `food_class` (
  `food_class_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜品分类id',
  `food_class_name` varchar(32) DEFAULT NULL COMMENT '分类名',
  PRIMARY KEY (`food_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `food_class` */

/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) NOT NULL COMMENT '用户微信号',
  `order_time` int(11) NOT NULL COMMENT '订餐时间',
  `order_number` int(11) DEFAULT NULL COMMENT '取餐号',
  `order_statue` int(11) DEFAULT NULL COMMENT '订单状态（0：已下单 1：已结单）',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定餐表';

/*Data for the table `order` */

/*Table structure for table `order_food` */

DROP TABLE IF EXISTS `order_food`;

CREATE TABLE `order_food` (
  `order_food_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `food_id` int(11) DEFAULT NULL COMMENT '菜品id',
  PRIMARY KEY (`order_food_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `order_food` */

/*Table structure for table `restaurant` */

DROP TABLE IF EXISTS `restaurant`;

CREATE TABLE `restaurant` (
  `restaurant_id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_name` varchar(50) NOT NULL COMMENT '食堂名',
  PRIMARY KEY (`restaurant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='餐厅表';

/*Data for the table `restaurant` */

/*Table structure for table `stu_user` */

DROP TABLE IF EXISTS `stu_user`;

CREATE TABLE `stu_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) NOT NULL COMMENT '微信编号',
  `nickname` varchar(50) NOT NULL COMMENT '昵称',
  `sex` int(1) NOT NULL COMMENT '性别',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `student_id` varchar(50) DEFAULT NULL COMMENT '学号',
  `headimgurl` text NOT NULL COMMENT '用户头像',
  `grade` int(4) DEFAULT NULL COMMENT '年级',
  `major` varchar(32) DEFAULT NULL COMMENT '专业',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `stu_user` */

insert  into `stu_user`(`user_id`,`openid`,`nickname`,`sex`,`name`,`student_id`,`headimgurl`,`grade`,`major`) values (1,'o-7cE0dyHwbS2x4df1wwDMcHQ44w','海洋',1,NULL,NULL,'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLej3BJMxkwSic9piaMcvZ6oShmuCPSOqicS1BxGat0eH3iaTyibB8NmFGAnxUouARPDvesFcGesZqs2rg/0',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
